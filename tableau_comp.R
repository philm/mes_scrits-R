
                                        # Création de tableaux de comparaison sur un data frame selon la valeur d'une variable
                                        # tt : dat.frame
                                        # dcd : variable de tri
                                        #
                                        # Philippe MICHEL
                                        # philippe@docteur-michel.fr
                                        # 16 août 2016
                                        # Licence GNU
                                        #
                                        #
                                        # Test non paramétrique pour les valeurs numériques. Présentation des résultats en médiane /quartiles.
                                        #
tabwilc <- function(tt,dcd){
    library(xtable)
    ldcd <- length(levels(dcd))# Nb de modalité de la variable de tri
    mlig <- matrix(c(" ",levels(dcd)," "),nrow=1) # première ligne                 #
    # Le data.frame variable par variable
       for(l in 1:length(tt)){
       print(l)
        var <- tt[,l]  #Pour simplifier l'écriture
        dlig <- names(tt[l]) # Initialisation de la ligne avec le nom de la variable
        #
    if (is.numeric(var)){
        qo <- quantile(var[dcd=="oui"],na.rm=T)
         qn <- quantile(var[dcd=="non"],na.rm=T)
         mo <- paste0(qo[3]," [",qo[2],"-",qo[4],"]")
         mn <- paste0(qn[3]," [",qn[2],"-",qn[4],"]")
        dlig <- c(dlig,mo,mn) # On allonge la ligne
    ll <- wilcox.test(var~dcd)
    ll <- ll$p.value
    lla <- signif(ll,3)
    dlig <- c(dlig,lla)
      mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
  }
    else #Pour les variables discrètes
        {
       var <- as.factor(var)
       tb <- table(dcd,var) # on crée un tableau croisé
       tbm <- margin.table(tb,2) # Sommes par ligne      
       try(clig<- chisq.test(var,dcd,correct=F)$p.value) # Calcul du Chi2
       print("####### = ",clig)
           clig <- signif(clig,3)
       if (length(levels(var))==2){ #Variables à 2 niveaux         
           if(levels(var)[1]!="oui"){ # Complément du nom de variable
           dlig <- paste0(dlig, " (",levels(var)[1],")")
       }
           for (j in 1:ldcd){
            nn <- tb[j,1]
            pn <- signif(100*nn/tbm[j],3)
            dlig <- c(dlig,paste0(nn," (",pn,"%)"))   }    
          dlig <- c(dlig,clig)
          mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
                }
            else{ #Variables à plus de 2  niveaux
    tlig <- c(names(tt[l]),rep(" ",ldcd),clig)
    mlig <- rbind(mlig,tlig)
    for (i in 1:length(levels(var))){ #Pour chaque niveau de la variable
        dlig <- paste0("~~~~~",levels(var)[i])
        for (j in 1:ldcd){
            nn <- tb[j,i]
            pn <- signif(100*nn/tbm[j],3)
            dlig <- c(dlig,paste0(nn," (",pn,"%)"))
        }
        dlig <- c(dlig," ")
        mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
    }
}
        
}}
        xlig <- xtable(mlig) # Transformation du tableau en LaTeX
    print(xlig,sanitize.text.function = function(x){x},include.rownames=FALSE, booktabs=TRUE,floating=FALSE)
   
}
                                        #
                                        #
                                        #
                                        #
                                        # Test t de Student. présentation des donénes numériques en moyenne +- écrat-type.
                                        #
tabmulti <- function(tt,dcd){
    library(xtable)
    ldcd <- length(levels(dcd))# Nb de modalité de la variable de tri
    mlig <- c() # première ligne                 #
    # Le data.frame variable par variable
       for(l in 1:length(tt)){
           print(l)
        var <- tt[,l]  #Pour simplifier l'écriture
        dlig <- names(tt[l]) # Initialisation de la ligne avec le nom de la variabl
        #
           if (is.numeric(var)){
               print("~~~~~~~~~~~~~~~")
    agm <- aggregate(var,by=list(dcd),FUN="mean",na.rm=T)[2]
    ags <- aggregate(var,by=list(dcd),FUN="sd",na.rm=T)[2]
    for (j in 1:ldcd){ #pour chaque modalité
        moy <- round(agm[j,1],2)
        etyp <- round(ags[j,1],2)
        mm <- paste0(moy," ± ",etyp)
        dlig <- c(dlig,mm) # On allonge la ligne
    }
    ll <- anova(lm(var~dcd))[5]
    lla <-ll[1,1]
               lla <- signif(lla,3)
               print(lla)
               dlig <- c(dlig,lla)
               print(dlig)
      mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
  }
    else #Pour les variables discrètes
        {
            var <- as.factor(var)
            # print("xxxxxxxxxx")
       tb <- table(dcd,var) # on crée un tableau croisé
       tbm <- margin.table(tb,2) # Sommes par ligne
       #if (tbm[1] & tbm[2] == TRUE){
       clig<- chisq.test(var,dcd,correct=T)$p.value # Calcul du Chi2
                                        clig <- signif(clig,3)
       if (length(levels(var))==2){ # Variables à 2 niveaux         
           if(levels(var)[1]!="oui"){ # Complément du nom de variable
           dlig <- paste0(dlig, " (",levels(var)[1],")")
       }
           for (j in 1:ldcd){
            nn <- tb[j,1]
            pn <- signif(100*nn/tbm[j],3)
            dlig <- c(dlig,paste0(nn," (",pn,"\\,\\%)"))   }    
          dlig <- c(dlig,clig)
          mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
                }
            else{ #Variables à plus de 2  niveaux
    tlig <- c(names(tt[l]),rep(" ",ldcd),clig)
    mlig <- rbind(mlig,tlig)
    for (i in 1:length(levels(var))){ #Pour chaque niveau de la variable
        dlig <- paste0("~~~~~",levels(var)[i])
        for (j in 1:ldcd){
            print(j)
            nn <- tb[j,i]
            print(paste(nn,tbm[i]))
            pn <- signif(100*nn/tbm[i],3)
            dlig <- c(dlig,paste0(nn,"/",tbm[i]," (",pn,"\\,\\%)"))
        }
        dlig <- c(dlig," ")
        mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
    }
}
        
}}
    xlig <- xtable(mlig) # Transformation du tableau en LaTeX
       ttit <- paste0("}&\\text{",levels(dcd),collapse="")# titre du tableau
    ttit <- paste0("{ ",ttit,"}&p\\\\",collapse="")
    #
    print(xlig,
          align = "lccc",
          tabular.environment='longtable',
          include.colnames=FALSE,
          floating=FALSE,
          booktabs=TRUE,
          hline.after=-1,
          include.rownames=FALSE,
          sanitize.text.function = function(x){x},
          add.to.row = list(pos = list(0),
                            command = paste0(ttit,"
  \\midrule
\\endfirsthead
  \\midrule "
,ttit,
"
\\midrule
\\endhead
\\bottomrule
\\endfoot
\\bottomrule
\\caption{} 
\\label{tab}
\\endlastfoot
 ")))
    }


                                        #

                                       #
                                        #
                                        #
                                        # Test t de Student. présentation des donénes numériques en moyenne +- écart-type.
                                        #
tabx <- function(tt,dcd){
    library(xtable)
    ldcd <- length(levels(dcd))# Nb de modalité de la variable de tri
    mlig <- matrix(c(" ",levels(dcd)," "),nrow=1) # première ligne                 #
    # Le data.frame variable par variable
       for(l in 2:length(tt)){
       print(l)
        var <- tt[,l]  #Pour simplifier l'écriture
        dlig <- names(tt[l]) # Initialisation de la ligne avec le nom de la variabl
        #
    if (is.numeric(var)){
    agm <- aggregate(var,by=list(dcd),FUN="mean",na.rm=T)[2]
    ags <- aggregate(var,by=list(dcd),FUN="sd",na.rm=T)[2]
    for (j in 1:ldcd){ #pour chaque modalité
        moy <- round(agm[j,1],2)
        etyp <- round(ags[j,1],2)
        mm <- paste0(moy,"\\pm",etyp)
        dlig <- c(dlig,mm) # On allonge la ligne
    }
    #ll <- anova(lm(var~dcd))[5]
    #lla <-ll[1,1]
                                        #lla <- signif(lla,3)
    ll <- glm(dcd~var,family="binomial")
    lla<-drop1(ll,test="Chisq")[[5]][2]
    lla <- signif(lla,3)
    dlig <- c(dlig,lla)
      mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
  }
    else #Pour les variables discrètes
        {
       var <- as.factor(var)
       tb <- table(dcd,var) # on crée un tableau croisé
       tbm <- margin.table(tb,2) # Sommes par ligne      
       clig <- chisq.test(var,dcd,correct=F)$p.value  # Calcul du Chi2
           clig <- signif(clig,3)
       if (length(levels(var))==2){ #Variabmes à 2 niveaux         
           if(levels(var)[1]!="oui"){ # Complément du nom de variable
           dlig <- paste0(dlig, " (",levels(var)[1],")")
       }
           for (j in 1:ldcd){
            nn <- tb[j,1]
            pn <- signif(100*nn/tbm[j],3)
            dlig <- c(dlig,paste0(nn," (",pn," \\,\\%)"))   }    
          dlig <- c(dlig,clig)
          mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
                }
            else{ #Variables à plus de 2  niveaux
    tlig <- c(names(tt[l]),rep(" ",ldcd),clig)
    mlig <- rbind(mlig,tlig)
    for (i in 1:length(levels(var))){ #Pour chaque niveau de la variable
        dlig <- paste0("~~~~~",levels(var)[i])
        for (j in 1:ldcd){
            nn <- tb[j,i]
            pn <- signif(100*nn/tbm[j],3)
            dlig <- c(dlig,paste0(nn," (",pn,"\\,\\%)"))
        }
        dlig <- c(dlig," ")
        mlig <- rbind(mlig,dlig) #ajout de la ligne au tableau
    }
}
        
        }}
    ttit <- paste0("}&\\text{",levels(dcd),collapse="")
    ttit <- paste0("{ ",ttit,"}\\\\",collapse="")
    print(ttit)
        xlig <- xtable(mlig) # Transformation du tableau en LaTeX
    print(xlig,
          tabular.environment='longtable',
          include.colnames=FALSE,
          floating=FALSE,
          booktabs=TRUE,
          hline.after=-1,
          include.rownames=FALSE,
          sanitize.text.function = function(x){x}
          )
    }


                                        #
